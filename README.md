
# Step by step instructions to become a react/node.js web developer from Scratch.

[[_TOC_]]

## 1. Introduction 
This material provides a guide and resources to become a react/node.js developer. The text and links are gotten from external sources, mostly [FreeCodeCamp](https://www.freecodecamp.org/) and other links which are helpful to learn programming. The aspects covered here are HTML, CSS, Javascript, Git, PostgreSQL, React.js, and Node.js, and Typescript. If you are already comfortable with Javascript, CSS, and HTML, you can skip to the  #Development section. 

### 1.1 What is computer programming?

A computer program consists of code that is executed on a computer to perform particular tasks. This code is written by programmers.
Programming is the process of giving machines a set of instructions that describe how a program should be carried out. [Continue reading on freecodecamp](https://www.freecodecamp.org/news/what-is-programming/)

### 1.2 What is Web development
Web development is the act of building and maintaining websites for the internet or intranet (private network). Examples of websites you might build would include blogs, personal pages, E-commerce sites, or social media sites. [Continue reading on freecodecamp](https://www.freecodecamp.org/news/what-is-web-development-how-to-become-a-web-developer-career-path/)


### 1.3 HTML
#### What is HTML?
HTML, or Hypertext Markup Language, is a markup language for the web that defines the structure of web pages. It is one of the most basic building blocks of every website, so it's crucial to learn if you want to have a career in web development.[Continue reading on freecodecamp](https://www.freecodecamp.org/news/what-is-html-definition-and-meaning/)

#### HTML Practice on [W3school](https://www.w3schools.com/html/html_basic.asp)
W3school gives an interractive learning experience for web programming languages. It contains learning resources and a platform to write and run HMTL code. At any point of the lessons, click on *Try it yourself* to code and see the results. 

![W3school Try it yourself button](./image-2.png)

Edit on the left and press the run button to run the code

![W3school Run button](./image-3.png)

At the bottom of each page, there are *Next* and *Previous* buttons. Click on *Next* to go to the next tutorial. Continue until [HTML Examples](https://www.w3schools.com/html/html_accessibility.asp).

![image-4.png](./image-4.png)

[Continue course on W3schools](https://www.w3schools.com/html/html_basic.asp)


### 1.4 CSS
#### What is CSS?
CSS (Cascading Style Sheets) is what makes web pages look good and presentable. It’s an essential part of modern web development and a must-have skill for any web designer and developer.[Continue reading on freecodecamp](https://www.freecodecamp.org/news/get-started-with-css-in-5-minutes-e0804813fc3e/)

#### CSS Practice on [W3school](https://www.w3schools.com/css/css_intro.asp)
Start the course with [css intro](https://www.w3schools.com/css/css_intro.asp) and follow it up to [CSS Examples](https://www.w3schools.com/css/css_exercises.asp)


### 1.5 Javascript
#### What is Javascript?
JavaScript is used with HTML and CSS to create dynamic and interactive web pages and mobile applications. We often call it one of the building blocks of web development.
[Continue reading on freecodecamp](https://www.freecodecamp.org/news/what-is-javascript-javascript-code-explained-in-plain-english)


#### JavaScript Practice on [W3school](https://www.w3schools.com/js/js_intro.asp)
[W3Schools Javascript course](https://www.w3schools.com/js/js_intro.asp). Follow the lessons up to [JS JSONP](https://www.w3schools.com/js/js_json_jsonp.asp)

[Video alternative](https://www.youtube.com/watch?v=PkZNo7MFNFg).For the video alternative, you can sign up on the following sites to write and test code online 
[Codepen](https://codepen.io/pen/) Direct html, css, javascript editing. 
[Scrimba](https://scrimba.com/new) Create a variety of web projects. 



### Online Project with HTML, CSS, and JavaScript

The project builds a calculator using what has been covered in HTML, CSS, and JavaScript. The calculator will work as the gif below. It is advisable to try building this on your own and then follow the tutorials on FreeCodpeCamp to see how it was built. 
![image-5.png](./image-5.png)
Follow along and build project with freecodecamp [Project description here](https://www.freecodecamp.org/news/how-to-build-an-html-calculator-app-from-scratch-using-javascript-4454b8714b98/)

#### Other practice projects

If you want to practise with more projects, The link below contains some example projects good for beginners.  [Top 15+ JavaScript Projects](https://www.interviewbit.com/blog/javascript-projects/)



## 2 Development

This section contains tools and packages which will help a developer start building projects.

### Git
### What is Git
Git is a version control system that developers use all over the world. It helps you track different versions of your code and collaborate with other developers. If you are working on a project over time, you may want to keep track of which changes were made, by whom, and when those changes were made. This becomes increasingly important if you end up having a bug in your code! Git can help you with this. [Continue reading on freecodecamp](https://www.freecodecamp.org/news/what-is-git-learn-git-version-control/)

### Git Learning resources

- [Git Crash Course](https://www.youtube.com/watch?v=RGOj5yH7evk)
- [Git Cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)
- [Git Intermediate Course](https://www.youtube.com/watch?v=Uszj_k0DGsg)

### Javascript development environment
To run javascript locally, you will need node.js and npm installed. Also, you will be using the  VSCode IDE(Integrated Development Environment)

#### Node.js and NPM
You can download and install node.js from [the official website](https://nodejs.org/en/download/) or follow the tutorial below.
[How to Install Node.js and npm on Windows](https://www.freecodecamp.org/news/how-to-install-node-js-and-npm-on-windows-2/)

#### Vscode

You can download and install VSCode from [the official website](https://code.visualstudio.com/download). You can also follow [this tutorial](https://www.freecodecamp.org/news/vscode-react-setup/) to see how to setup visual studio code for React development. 


### React
#### What is React

React (also known as React.js or ReactJS) is a free and open-source front-end JavaScript library for creating UI component-based user interfaces.
[Continue reading on FreeCodecamp](https://www.freecodecamp.org/news/get-started-with-react-for-beginners/)

Follow the full course on Node.js on Freecodecamp's youtube channel. Again, it is advisable to try out the projects in the course before checking the code. [React Course - Beginner's Tutorial for React JavaScript Library](https://www.youtube.com/watch?v=bMknfKXIFA8&t=4s)


### Node.js
#### What is Node.js
Node.js is a JavaScript runtime that extends its capability to the server-side. It is built on Chrome’s V8 JavaScript Engine.
[Continue reading on freecodecamp](https://www.freecodecamp.org/news/introduction-to-nodejs/)

Follow the full course on Node.js on Freecodecamp's youtube channel.[Full course on Node.js](https://www.youtube.com/watch?v=Oe421EPjeBE&t=1s)


### PostgreSQL

#### What is Postgres
PostgreSQL is a powerful, open source object-relational database system that uses and extends the SQL language combined with many features that safely store and scale the most complicated data workloads. The origins of PostgreSQL date back to 1986 as part of the POSTGRES project at the University of California at Berkeley and has more than 30 years of active development on the core platform. [Continue reading on Official website](https://www.postgresql.org/about/)

#### Courses
- [FreeCodeCamp - How to Manage PostgreSQL Databases from the Command Line with psql](https://www.freecodecamp.org/news/manage-postgresql-with-psql/)

- [FreeCodeCamp- Full Course for Beginners](https://www.youtube.com/watch?v=qw--VYLpxG4)



## Advanced
### Typescript
#### What is Typescript?
TypeScript is a typed superset of JavaScript that compiles to plain JavaScript. TypeScript is a superset of the JavaScript language that has a single open-source compiler and is developed mainly by a single vendor: Microsoft. The goal of TypeScript is to help catch mistakes early through a type system and to make JavaScript development more efficient.
[Continue reading on Stackoverflow](https://stackoverflow.com/a/35048303)

[Typescript Cheatsheet](https://www.typescriptlang.org/cheatsheets)

[Typescript beginner tutorial - Youtube](https://www.youtube.com/watch?v=BwuLxPH8IDs)
